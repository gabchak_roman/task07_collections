package com.gabchak.binaryTreeMap;

public class Main {
    public static void main(String[] args) {
        BinaryTreeMap<Integer, String> treeMap = new BinaryTreeMap<>((a, b) -> a - b);
        treeMap.put(10, "A");
        treeMap.put(5, "B");
        treeMap.put(3, "C");
        treeMap.put(20, "D");
        treeMap.put(25, "E");
        treeMap.put(25, "EEE");
        treeMap.put(8, "F");

        System.out.println("get 10 -> " + treeMap.get(10));
        System.out.println("get 5 -> " + treeMap.get(5));
        System.out.println("get 3 -> " + treeMap.get(3));
        System.out.println("get 20 -> " + treeMap.get(20));
        System.out.println("get 25 -> " + treeMap.get(25));
        System.out.println("get 8 -> " + treeMap.get(8));
        System.out.println("get 100 -> " + treeMap.get(100));

        treeMap.print();
        System.out.println("Remove 10");
        treeMap.remove(10);
        treeMap.print();
        System.out.println("Remove 25");
        treeMap.remove(25);
        treeMap.print();
        System.out.println("Remove 5");
        treeMap.remove(5);
        treeMap.print();
        System.out.println("Remove 3");
        treeMap.remove(3);
        treeMap.print();
        System.out.println("Remove 20");
        treeMap.remove(20);
        treeMap.print();
        System.out.println("Remove 8");
        treeMap.remove(8);
        treeMap.print();
        System.out.println("Remove 100");
        treeMap.remove(100);
        treeMap.print();
    }
}