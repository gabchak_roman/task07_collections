package com.gabchak.binaryTreeMap;

import java.util.Comparator;

class BinaryTreeMap<K, V> {
    private final Comparator<K> comparator;
    private TreeNode<K, V> root;

    BinaryTreeMap(Comparator<K> comparator) {
        this.comparator = comparator;
    }

    public void put(K key, V value) {
        if (root == null) {
            root = new TreeNode<>(key, value);
            return;
        }
        putRecursive(root, key, value);
    }

    public V get(K key) {
        TreeNode<K, V> node = getRecursive(root, key);
        if (node == null) {
            return null;
        }
        return node.value;
    }

    public void remove(K key) {
        root = removeRecursive(root, key);
    }

    public void print() {
        printRecursive(root);
    }

    private TreeNode<K, V> putRecursive(TreeNode<K, V> node, K key, V value) {
        if (node == null) {
            return new TreeNode<>(key, value);
        }
        int comparison = comparator.compare(node.key, key);
        if (comparison < 0) {
            node.right = putRecursive(node.right, key, value);
        }
        else if (comparison > 0) {
            node.left = putRecursive(node.left, key, value);
        }
        else {
            node.key = key;
            node.value = value;
        }
        return node;
    }

    private TreeNode<K, V> getRecursive(TreeNode<K, V> node, K key) {
        if (node == null) {
            return null;
        }
        int comparison = comparator.compare(node.key, key);
        if (comparison == 0) {
            return node;
        }
        if (comparison < 0) {
            return getRecursive(node.right, key);
        }
        return getRecursive(node.left, key);
    }

    private TreeNode<K, V> removeRecursive(TreeNode<K, V> node, K key) {
        if (node == null) {
            return null;
        }
        int comparison = comparator.compare(node.key, key);
        if (comparison < 0) {
            node.right = removeRecursive(node.right, key);
        }
        else if (comparison > 0) {
            node.left = removeRecursive(node.left, key);
        }
        else {
            if (node.left == null) {
                return node.right;
            }
            if (node.right == null) {
                return node.left;
            }
            TreeNode<K, V> minOnRight = minNode(node.right);
            node.key = minOnRight.key;
            node.value = minOnRight.value;
            node.right = removeRecursive(node.right, node.key);
        }
        return node;
    }

    private TreeNode<K, V> minNode(TreeNode<K, V> node)  {
        while (node.left != null) {
            node = node.left;
        }
        return node;
    }

    private void printRecursive(TreeNode<K, V> node) {
        if (node == null) {
            return;
        }
        printRecursive(node.left);
        System.out.println(node.key + " -> " + node.value);
        printRecursive(node.right);
    }

    private class TreeNode<K, V> {
        K key;
        V value;
        TreeNode<K, V> left;
        TreeNode<K, V> right;

        private TreeNode(K key, V value) {
            this.key = key;
            this.value = value;
        }
    }

}