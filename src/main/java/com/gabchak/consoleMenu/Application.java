package com.gabchak.consoleMenu;

import com.gabchak.consoleMenu.controller.Controller;
import com.gabchak.consoleMenu.controller.ControllerImpl;
import com.gabchak.consoleMenu.model.Model;
import com.gabchak.consoleMenu.model.Users;
import com.gabchak.consoleMenu.view.View;
import com.gabchak.consoleMenu.view.ViewImpl;

public class Application {
    public static void main(String[] args) {

        View view = new ViewImpl();
        Model model = new Users();
        Controller controller = new ControllerImpl(model, view);
        controller.start();

    }
}
