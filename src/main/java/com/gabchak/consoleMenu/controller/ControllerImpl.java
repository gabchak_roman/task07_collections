package com.gabchak.consoleMenu.controller;

import com.gabchak.consoleMenu.model.Gender;
import com.gabchak.consoleMenu.model.Model;
import com.gabchak.consoleMenu.model.User;
import com.gabchak.consoleMenu.view.View;

import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.Map;

public class ControllerImpl implements Controller, Print {

    private Map<String, String> menu;
    private Map<String, Print> methodsMenu;

    private Model model;
    private View view;

    public ControllerImpl(Model modelIn, View viewIn) {
        this.model = modelIn;
        this.view = viewIn;
    }

    public void start() {
        menu();
        print();
    }

    public void menu() {
        menu = new LinkedHashMap<>();

        menu.put("1", "|         1  -  Add user.                |");
        menu.put("2", "|         2  -  Delete user.             |");
        menu.put("3", "|         3  -  Show users.              |");
        menu.put("4", "|         4  -  Sort users by name.      |");
        menu.put("5", "|         5  -  Sort users by age.       |");
        menu.put("Q", "|         Q  -  Exit                     |");

        methodsMenu = new LinkedHashMap<>();
        methodsMenu.put("1", this::pressButton1);
        methodsMenu.put("2", this::pressButton2);
        methodsMenu.put("3", this::pressButton3);
        methodsMenu.put("4", this::pressButton4);
        methodsMenu.put("5", this::pressButton5);

    }

    private void pressButton1() {
        view.printMessage("Enter name: ");
        String name = view.getInput();
        name = name.substring(0, 1).toUpperCase() + name.substring(1);
        view.printMessage("Enter surname: ");
        String surname = view.getInput();
        surname = surname.substring(0, 1).toUpperCase() + surname.substring(1);
        String temp;
        view.printMessage("Enter Age: ");
        do {
            temp = view.getInput();
        } while (!temp.matches("[0-9]{1,3}"));
        int age = Integer.parseInt(temp);

        view.printMessage("Choose gender 'm' -male / 'f' -female: ");
        String gender = view.getInput();
        if (gender.equalsIgnoreCase("m")){
            gender = String.valueOf(Gender.MALE);
        }
        else if (gender.equalsIgnoreCase("f")){
            gender = String.valueOf(Gender.FEMALE);
        }
        else {
            gender = "unknown";
        }
        model.addUser(name, surname, age, gender);
    }

    private void pressButton2() {
        view.printMessage("Enter user number to delete: ");
        try {
            model.deleteUser(Integer.parseInt(view.getInput()));
        } catch (NumberFormatException e) {
            view.printMessage("Wrong input");
        }
    }

    private void pressButton3() {
        view.printInfo(model);
    }

    private void pressButton4() {
        model.getList().sort(Comparator.comparing(User::getName));
    }

    private void pressButton5() {
        model.getList().sort(Comparator.comparing(User::getAge));
    }

    public final void print() {
        String keyMenu;
        do {
            outputMenu();
            view.printMessage("▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬");
            view.printMessage("Please, select menu point.  ");
            keyMenu = view.getInput().toUpperCase();
            try {
                methodsMenu.get(keyMenu).print();
            } catch (Exception ignored) {
            }
        } while (!keyMenu.equals("Q"));
    }

    private void outputMenu() {
        view.printMessage("►◄►◄►◄►◄►◄►◄►◄►◄►◄►◄►◄►◄►◄►◄►◄►◄►◄►◄►◄►◄►◄");
        view.printMessage("                  MENU:");
        view.printMessage("►◄►◄►◄►◄►◄►◄►◄►◄►◄►◄►◄►◄►◄►◄►◄►◄►◄►◄►◄►◄►◄");
        menu.values().forEach(message -> view.printMessage(message));
    }
}
