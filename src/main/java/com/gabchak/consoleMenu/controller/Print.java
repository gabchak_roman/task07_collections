package com.gabchak.consoleMenu.controller;

@FunctionalInterface
public interface Print {
    void print();
}
