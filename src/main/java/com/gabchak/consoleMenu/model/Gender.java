package com.gabchak.consoleMenu.model;

public enum Gender {
    MALE, FEMALE
}
