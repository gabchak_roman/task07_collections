package com.gabchak.consoleMenu.model;

import java.util.List;

public interface Model {
    List<User> getList();

    void deleteUser(int number);

    void addUser(String name, String surname, int age, String gender);

    void addUserList(List<User> userList);
}
