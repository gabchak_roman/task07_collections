package com.gabchak.consoleMenu.model;

import java.util.ArrayList;
import java.util.List;

public class Users implements Model {

    private List<User> userList = new ArrayList<>();

    @Override
    public List<User> getList() {
        return userList;
    }

    @Override
    public void deleteUser(int number) {
        if ((number - 1) < 0 || (number - 1) > userList.size()) {
            throw new ArrayIndexOutOfBoundsException();
        }
        userList.remove(number);
    }

    @Override
    public void addUser(String name, String surname, int age, String gender) {
        userList.add(new User(name, surname, age, gender));
    }

    @Override
    public void addUserList(List<User> userList) {
        this.userList = userList;
    }
}
