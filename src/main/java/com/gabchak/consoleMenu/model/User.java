package com.gabchak.consoleMenu.model;

import java.util.Comparator;

public class User {

    private static int userId;
    private final int ID;
    private String name;
    private String surname;
    private int age;
    private String gender;


    User(String name, String surname, int age, String gender) {
        ID = ++userId;
        this.name = name;
        this.surname = surname;
        this.age = age;
        this.gender = gender;
    }

    public String getGender() {
        return gender;
    }

    public String getSurname() {
        return surname;
    }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    @Override
    public String toString() {
        return "User №" + ID
                + "  Surname: " + surname
                + "  Name: " + name
                + "  Age: " + age
                + "  Gender: " + gender;

    }
}
