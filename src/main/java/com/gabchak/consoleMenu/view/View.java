package com.gabchak.consoleMenu.view;

import com.gabchak.consoleMenu.model.Model;

public interface View {

    String getInput();

    void printMessage(String message);

    void printInfo(Model model);
}
