package com.gabchak.consoleMenu.view;

import com.gabchak.consoleMenu.model.Model;

import java.util.Scanner;

public class ViewImpl implements View{

    private Scanner input = new Scanner(System.in);

    @Override
    public final String getInput() {
        return input.nextLine();
    }

    @Override
    public final void printMessage(final String message) {
        System.out.println(message);
    }

    @Override
    public void printInfo(Model users) {
        users.getList().forEach(System.out::println);
    }
}